//
//  _0220628_FroylanAlmeida_NYCSchoolsApp.swift
//  20220628-FroylanAlmeida-NYCSchools
//
//  Created by Froy on 6/28/22.
//

import SwiftUI

@main
struct _0220628_FroylanAlmeida_NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
