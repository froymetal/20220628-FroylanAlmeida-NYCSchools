//
//  NetworkManager.swift
//  20220628-FroylanAlmeida-NYCSchools
//
//  Created by Froy on 6/29/22.
//

import Combine
import Foundation

protocol NetworkService {
    func getModel<T: Decodable>(from url: URL?) -> AnyPublisher<T, Error>
}

class NetworkManager: NetworkService {

    func getModel<T: Decodable>(from url: URL?) -> AnyPublisher<T, Error> {

        guard let url = url else {
            return Fail(error: NetworkError.badURL).eraseToAnyPublisher()
        }
        
        return URLSession.shared.dataTaskPublisher(for: url)
            .tryMap { map in
                if let hResponse = map.response as? HTTPURLResponse, !(200..<300).contains(hResponse.statusCode) {
                    throw NetworkError.badResponseCode
                }
                return map.data
            }
            .decode(type: T.self, decoder: JSONDecoder())
            .mapError { _ in
                return NetworkError.other
            }
            .eraseToAnyPublisher()
    }

}
