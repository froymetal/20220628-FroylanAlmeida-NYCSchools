//
//  NetworkError.swift
//  20220628-FroylanAlmeida-NYCSchools
//
//  Created by Froy on 6/29/22.
//

import Foundation

enum NetworkError: Error {
    case badURL
    case badResponseCode
    case decodeFailure
    case other

    var description: String {
        switch self {
        case .badURL:
            return "URL to remote is not correct."
        case .badResponseCode:
            return "Could not reach cloud."
        case .decodeFailure:
            return "Unexpected data from cloud."
        default:
            return "Whoops, something went wrong."
        }
    }
}
