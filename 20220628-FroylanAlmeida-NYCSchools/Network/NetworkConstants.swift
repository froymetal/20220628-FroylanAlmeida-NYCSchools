//
//  NetworkConstants.swift
//  20220628-FroylanAlmeida-NYCSchools
//
//  Created by Froy on 6/29/22.
//

import Foundation

struct NetworkConstants {
    static let schoolsUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let schoolDetailsUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="
}
