//
//  Schools.swift
//  20220628-FroylanAlmeida-NYCSchools
//
//  Created by Froy on 6/29/22.
//

import Foundation


struct SchoolsData: Decodable, Hashable {
    var dbn: String
    var name: String
    var overview: String

    enum CodingKeys: String, CodingKey {
        case name = "school_name"
        case overview = "overview_paragraph"
        case dbn
    }
}


struct SchoolsDataDetails: Decodable {
    var dbn: String
    var name: String
    var numberOfTest: String
    var satReading: String
    var satAvg: String
    var satWriting: String

    enum CodingKeys: String, CodingKey {
        case dbn
        case name = "school_name"
        case numberOfTest = "num_of_sat_test_takers"
        case satReading = "sat_critical_reading_avg_score"
        case satAvg = "sat_math_avg_score"
        case satWriting = "sat_writing_avg_score"
    }




}
