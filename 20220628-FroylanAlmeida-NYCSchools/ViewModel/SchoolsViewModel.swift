//
//  SchoolViewModel.swift
//  20220628-FroylanAlmeida-NYCSchools
//
//  Created by Froy on 6/29/22.
//

import Foundation
import Combine

class SchoolsViewModel: ObservableObject {

    private let networkService: NetworkService
    @Published private(set) var schools: [SchoolsData] = []
    @Published private(set) var schoolsDetails: [String: SchoolsDataDetails] = [:]
    private var subscribers = Set<AnyCancellable>()

    init(networkService: NetworkService) {
        self.networkService = networkService
        self.getSchools()
    }
    // Function to get the schools basic information
    func getSchools() {
        self.networkService.getModel(from: URL(string: NetworkConstants.schoolsUrl))
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .failure(let err):
                    // TODO: Make error more user friendly
                    print(err)
                case .finished:
                    break
                }
            } receiveValue: { [weak self] (schools: [SchoolsData]) in
                self?.schools = schools
            }
            .store(in: &self.subscribers)
    }

    // Function to get the schools details SAT information
    func fetchSchoolDetails (dbn: String) {

        // TODO: Guard if we already have the school details
        let urlDetails = "\(NetworkConstants.schoolDetailsUrl)\(dbn)"
        self.networkService.getModel(from: URL(string: urlDetails))
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .failure(let err):
                    // TODO: Make error more user friendly
                    print(err)
                case .finished:
                    break
                }
            } receiveValue: { [weak self] (schools: [SchoolsDataDetails]) in
                guard let school = schools.first else { return }
                self?.schoolsDetails[dbn] = school
            }
            .store(in: &self.subscribers)
    }
}
