//
//  SchoolsDetails.swift
//  20220628-FroylanAlmeida-NYCSchools
//
//  Created by Froy on 6/29/22.
//

import SwiftUI

struct SchoolsDetails: View {
    let dbn: String
    @ObservedObject var schoolViewModel: SchoolsViewModel

    init(viewModel: SchoolsViewModel, dbn: String) {
        self.schoolViewModel = viewModel
        self.dbn = dbn
        self.schoolViewModel.fetchSchoolDetails(dbn: dbn)
    }
    
    var body: some View {
        VStack {
            Text(self.schoolViewModel.schoolsDetails[dbn]?.name ?? "N/A")
                .font(.title)
                .font(Font.headline.weight(.bold))
                .padding()
                .foregroundColor(.blue)
            Image(systemName: "list.bullet.rectangle")
                .resizable()
                .frame(width: 100, height: 100, alignment: .center)
                .foregroundColor(.gray)
                .padding(.bottom, 25)
        }

        VStack {
            HStack {
                Text("Number of Tests : ")
                    .font(.system(size: 20, weight: .bold, design: .default))
                Spacer()
                Text("\(self.schoolViewModel.schoolsDetails[dbn]?.numberOfTest ?? "N/A")")
                    .font(.system(size: 20, weight: .thin, design: .default))
            }
            HStack {
                Text("SAT Reading Avg Score : ")
                    .font(.system(size: 20, weight: .bold, design: .default))
                Spacer()
                Text("\(self.schoolViewModel.schoolsDetails[dbn]?.satReading ?? "N/A")")
                    .font(.system(size: 20, weight: .thin, design: .default))
            }
            HStack {
                Text("SAT Writing Avg Score : ")
                    .font(.system(size: 20, weight: .bold, design: .default))
                Spacer()
                Text("\(self.schoolViewModel.schoolsDetails[dbn]?.satWriting ?? "N/A")")
                    .font(.system(size: 20, weight: .thin, design: .default))
            }
            HStack {
                Text("SAT Math Avg Score : ")
                    .font(.system(size: 20, weight: .bold, design: .default))
                Spacer()
                Text("\(self.schoolViewModel.schoolsDetails[dbn]?.satAvg ?? "N/A")")
                    .font(.system(size: 20, weight: .thin, design: .default))
            }
        }
        .padding([.leading, .trailing], 40)
        Spacer()
    }
}

struct SchoolsDetails_Previews: PreviewProvider {
    static var previews: some View {
        SchoolsDetails(viewModel: SchoolsViewModel(networkService: NetworkManager()), dbn: "12345")
    }
}
