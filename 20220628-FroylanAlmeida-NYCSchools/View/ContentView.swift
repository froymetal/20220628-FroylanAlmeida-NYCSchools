//
//  ContentView.swift
//  20220628-FroylanAlmeida-NYCSchools
//
//  Created by Froy on 6/28/22.
// glpat-dk5Zh9Kb9w_yg98xTgXZ

import SwiftUI

struct ContentView: View {

    @ObservedObject var schoolViewModel: SchoolsViewModel = SchoolsViewModel(networkService: NetworkManager())

    init() {
        // Change NavigationView features
        let navBarAppearance = UINavigationBar.appearance()
        navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.blue]
        navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.blue]
    }

    var body: some View {
        NavigationView {
            Form{
                List {
                    ForEach(self.schoolViewModel.schools, id: \.self) { school in
                        NavigationLink(destination: SchoolsDetails(viewModel: self.schoolViewModel, dbn: school.dbn)) {
                            HStack {
                                Text(Image.init(systemName: "building.columns"))
                                Text(school.name)
                            } .padding()
                        }
                    }
                }
            }
            .navigationTitle("NYC Schools List")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
